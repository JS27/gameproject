import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler {

    // Eigenschaften
    private Model model;

    // Konstruktoren
    public InputHandler(Model model) {
        this.model = model;
    }

    // Methoden

    public void onMouseClicked(int x) {
        model.spielzug(berechneSpalte(x), model.getAktuellerSpieler());
        System.out.print(model.getAktuellerSpieler());
        System.out.println("  " + model.getAktuellerSpieler().getAmountSpielsteine());
        model.spielerwechsel();
        System.out.println(x);
    }

    public int berechneSpalte(int x) {
        int spielfeldbreite = 0;
        if(x > 0 && x < 50){
            System.out.println(0);
            return 0;
        } else if(x>= 50 && x < 100){
            System.out.println(1);
            return 1;
        } else if (x>= 100 && x < 150){
            System.out.println(2);
            return 2;
        } else if(x>= 150 && x < 200){
            System.out.println(3);
            return 3;
        } else if(x>= 200 && x < 250){
            System.out.println(4);
            return 4;
        } else if (x>= 250 && x < 300){
            System.out.println(5);
            return 5;
        } else if (x>= 300 && x < 350){
            System.out.println(6);
            return 6;
        }
        else {
            return -1;
        }
    }
}
