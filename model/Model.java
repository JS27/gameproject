package model;

import java.util.LinkedList;
import java.util.List;

public class Model {

    // FINALS
    public static final int WIDTH = 350;
    public static final int HEIGHT = 300;

    // Eigenschaften
    private Spielfeld spielfeld;
    private Spieler spieler1;
    private Spieler spieler2;
    private Spieler aktuellerSpieler;



    // Konstruktoren
    public Model() {
        this.spielfeld = new Spielfeld();
        this.spieler1 = new Spieler("grün");
        this.spieler2 = new Spieler("blau");
        this.aktuellerSpieler = spieler1;
    }

    // Methoden
    public void spielzug(int spalte, Spieler spieler){
        Spielstein[][] s = this.spielfeld.getSpielsteine(); // 6 Zeilen, 7 Spalten
        for (int i = 5; i >= 0; i--){
            if(s[i][spalte] == null){
               s[i][spalte] = spieler.getSpielstein();
               return;
            }
        }
    }

    // Setter + Getter
    public Spieler getAktuellerSpieler() {
        return aktuellerSpieler;
    }

    public void spielerwechsel() {
        if(this.aktuellerSpieler == spieler1) {
            aktuellerSpieler = spieler2;
        } else if(this.aktuellerSpieler == spieler2){
            aktuellerSpieler = spieler1;
        }
    }

    public Spieler getSpieler1() {
        return spieler1;
    }

    public Spieler getSpieler2() {
        return spieler2;
    }

    public Spielfeld getSpielfeld() {
        return spielfeld;
    }


}
