package model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Spieler {
    private LinkedList<Spielstein> spielsteine;

    public Spieler(String farbe) {
        this.spielsteine = new LinkedList<>();
        for (int i = 0; i < 21; i++) {
            spielsteine.add(new Spielstein(farbe));
        }
        System.out.println(spielsteine.size());
    }

    public Spielstein getSpielstein(){
        return spielsteine.removeFirst();
    }

    public int getAmountSpielsteine(){
        return spielsteine.size();
    }
}
