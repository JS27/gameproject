package model;

import java.util.ArrayList;
import java.util.List;

public class Spielfeld {
    private Spielstein[][] spielsteine;

    public Spielfeld() {
        this.spielsteine = new Spielstein[6][7];
    }

    public Spielstein[][] getSpielsteine() {
        return spielsteine;
    }
}
