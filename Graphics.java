import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import model.Model;
import model.Spielstein;

import java.lang.reflect.Array;

public class Graphics {

    // Eigenschaften
    private Model model;
    private GraphicsContext gc;

    // Konstruktoren
    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    // Methoden
    // Ausgabe von dem Spielfeld
    public void draw() {

        // Clear Screen
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

        // Zeichne Spielfeld
        zeichneRaster();
        zeichneSpielfeld();


        // Zeichne Spielstein
        Spielstein[][] s = model.getSpielfeld().getSpielsteine(); // 6 Zeilen, 7 Spalten
        for (int i = 5; i >= 0; i--) {
            for (int k = 0; k <= 6; k++) {
                if (s[i][k] != null ) {
                    if (s[i][k].getFarbe() == "blau") {
                        gc.setFill(Color.BLUE);
                    }
                    else {
                        gc.setFill(Color.GREEN);
                    }
                    gc.fillOval(10 + k* 50, 10 + i * 50, 30, 30);
                }
            }

        }
    }

    private void zeichneSpielfeld() {
        gc.strokeRect(0, 0, 50, 50);
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(5);
        for (int k = 0; k < 6; k++) {
            for (int i = 0; i <= 7; i++) {
                gc.strokeOval((10 + i * 50), (10 + k * 50), 30, 30);
            }
        }
    }

    private void zeichneRaster() {
        gc.setFill(Color.BLACK);
        gc.setLineWidth(3);
        for (int k = 0; k < 6; k++) {
            for (int i = 0; i <= 7; i++) {
                gc.strokeRect((i * 50), (50 * k), 50, 50);
            }
        }
    }
}

